#!/bin/sh

# ------------------------------------------------------------
# The entry point is where we can perform some initialization
# tasks within the container. This will need to be adjusted
# for the specific needs of the customer.
# ------------------------------------------------------------

echo "Initializing container..."
echo "IAP_RUN_DIR: $IAP_RUN_DIR"
echo "MIGRATE_PROPERTIES: $MIGRATE_PROPERTIES"

# Run the migratePropertiesToDatabase script
# Extract the rabbitmq properties from the properties.json file. Those need to
# be handled differently. This block should only be executed for a "day-zero"
# install and could be risky to run in an established environment.
if [ -f "$IAP_RUN_DIR"/node_modules/@itential/pronghorn-core/migration_scripts/migratePropertiesToDatabase.js ] && [ "$MIGRATE_PROPERTIES" = "true" ]; then
  # Standard params required for rabbitmq
  rabbitmq_hosts=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.hosts | join(",")' )
  rabbitmq_port=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.port' )
  rabbitmq_admin_port=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.adminPort' )
  rabbitmq_protocol=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.protocol' )
  rabbitmq_username=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.username' )
  rabbitmq_password=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.password' )
  rabbitmq_vhost=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.vhost' )

  ## optional
  # If these are required it will be necessary to adjust the parameters used
  # in the migratePropertiesToDatabase script
  rabbitmq_key_path=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.keyPath' )
  rabbitmq_cert_path=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.certPath' )
  rabbitmq_passphrase=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.passphrase // ""' )
  rabbitmq_ca_path=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.caPath' )

  ## Set env vars for eventDeduplication / service_config_rabbitmq
  rabbitmq_dedup_active=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.deduplication.active' )
  rabbitmq_dedup_profile_props=$( < "$IAP_RUN_DIR"/properties.json jq -r '.rabbitmq.deduplication' )

  # Push all properties from the properties.json file into the container
  /usr/bin/node $IAP_RUN_DIR/node_modules/@itential/pronghorn-core/migration_scripts/migratePropertiesToDatabase.js unattended --userInputs port="$rabbitmq_port" protocol="$rabbitmq_protocol" username="$rabbitmq_username" password="$rabbitmq_password" hosts="$rabbitmq_hosts" vhost="$rabbitmq_vhost" || true
fi

echo "initialization complete.  Starting application..."

# This is executed what was defined in the CMD of the Dockerfile
exec "$@"
