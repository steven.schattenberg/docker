# RHEL7 Image
This will produce a RHEL 7 image.

## Useful commands
Build the container with all possible options:
```
docker build -t hsbc:example  \
 --build-arg JINJA2_VERSION=2.11.3 \
 --build-arg J2CLI_VERSION=0.3.10 \
 --build-arg TEXTFSM_VERSION=1.1.2 \
 --build-arg ITENTIAL_VERSION=itential-premium_2021.1.4 \
 --build-arg CUSTOMER=@hsbc \
 --build-arg LOGDIR=/var/log/pronghorn \
 --build-arg MIGRATE_PROPERTIES=true \
 --build-arg PYTHON_PACKAGE=rh-python38.x86_64 \
 --build-arg PIP_PACKAGE=rh-python38-python-pip.noarch \
 .
 ```
 Build the container with only the required options:
 ```
 docker build -t hsbc:example  \
 --build-arg ITENTIAL_VERSION=itential-premium_2021.1.4 \
 --build-arg CUSTOMER=@hsbc \
 .
 ```
 Run the container (day 2, skips migratePropertiesToDatabase.js):
 ```
 docker run -it --name HSBC -p 3000:3000 hsbc:example
 ```
 Run the container and migrate properties:
 ```
 docker run -it --name HSBC --env MIGRATE_PROPERTIES=true -p 3000:3000 hsbc:example
 ```
